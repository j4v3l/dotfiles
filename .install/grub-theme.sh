#!/bin/bash
cat <<"EOF"
  ____ ____  _   _ ____      _____ _   _ _____ __  __ _____ 
 / ___|  _ \| | | | __ )    |_   _| | | | ____|  \/  | ____|
| |  _| |_) | | | |  _ \ _____| | | |_| |  _| | |\/| |  _|  
| |_| |  _ <| |_| | |_) |_____| | |  _  | |___| |  | | |___ 
 \____|_| \_\\___/|____/      |_| |_| |_|_____|_|  |_|_____|
                                                            
EOF
echo "by J4V3L (2023)"
echo "-----------------------------------------------------"
echo "Installing GRUB Theme..."

# Function to install the Dark Matter GRUB theme
install_darkmatter_theme() {
    echo "Cloning Dark Matter GRUB theme..."
    git clone --depth 1 https://gitlab.com/VandalByte/darkmatter-grub-theme.git
    cd darkmatter-grub-theme

    echo "Please follow the on-screen instructions to complete the installation."
    sudo python3 darkmatter-theme.py -i

    echo "Installation process initiated. Please follow the prompts."
}

# Main execution flow
echo "Dark Matter GRUB Theme Installer"
install_darkmatter_theme
