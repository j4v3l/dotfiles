#!/bin/bash
cat <<"EOF"
 ____  ____  ____  __  __ 
/ ___||  _ \|  _ \|  \/  |
\___ \| | | | | | | |\/| |
 ___) | |_| | |_| | |  | |
|____/|____/|____/|_|  |_|
                          
EOF
echo "by J4V3L (2023)"
echo "-----------------------------------------------------"
echo "Installing SDDM..."
# Determine the directory where the script is located
SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

# Change the working directory to the parent directory of the script
PARENT_DIR="$SCRIPT_DIR/.."
cd "$PARENT_DIR" || exit 1

# Set the name of the log file to include the current date and time
LOG="install-$(date +%d-%H%M%S)_sddm.log"

ISAUR=$(command -v yay || command -v paru)

# Set the script to exit on error
set -e

# Function for installing packages
install_package() {
  if $ISAUR -Q "$1" &>>/dev/null; then
    echo "$1 is already installed. Skipping..."
  else
    echo "Installing $1 ..."
    $ISAUR -S --noconfirm "$1" 2>&1 | tee -a "$LOG"
    if ! $ISAUR -Q "$1" &>>/dev/null; then
      echo "$1 failed to install. Please check the install.log."
      exit 1
    fi
  fi
}

# SDDM
if pacman -Qs sddm >/dev/null; then
  read -n1 -rep "SDDM is already installed. Install sddm-git to remove it? (y/n)" manual_install_sddm
  echo
  if [[ $manual_install_sddm =~ ^[Yy]$ ]]; then
    $ISAUR -S sddm-git 2>&1 | tee -a "$LOG"
  fi
fi

echo "Installing SDDM-git..."
for package in sddm-git; do
  install_package "$package" 2>&1 | tee -a "$LOG"
  [ $? -ne 0 ] && {
    echo "$package install has failed, please check the install.log"
    exit 1
  }
done

for login_manager in lightdm gdm lxdm lxdm-gtk3; do
  if pacman -Qs "$login_manager" >/dev/null; then
    echo "disabling $login_manager..."
    sudo systemctl disable "$login_manager.service" 2>&1 | tee -a "$LOG"
  fi
done

echo "Activating sddm service..."
sudo systemctl enable sddm

sddm_conf_dir=/etc/sddm.conf.d
[ ! -d "$sddm_conf_dir" ] && {
  echo "$sddm_conf_dir not found, creating..."
  sudo mkdir "$sddm_conf_dir" 2>&1 | tee -a "$LOG"
}

wayland_sessions_dir=/usr/share/wayland-sessions
[ ! -d "$wayland_sessions_dir" ] && {
  echo "$wayland_sessions_dir not found, creating..."
  sudo mkdir "$wayland_sessions_dir" 2>&1 | tee -a "$LOG"
}
sudo cp assets/hyprland.desktop "$wayland_sessions_dir/" 2>&1 | tee -a "$LOG"

read -n1 -rep "Install SDDM themes? (y/n)" install_sddm_theme
if [[ $install_sddm_theme =~ ^[Yy]$ ]]; then
  while true; do
    read -rp "Which SDDM Theme to install? Catpuccin or Tokyo Night 'c' or 't': " choice
    case "$choice" in
    c | C)
      echo "Installing Catpuccin SDDM Theme"
      for sddm_theme in sddm-catppuccin-git; do
        install_package "$sddm_theme" 2>&1 | tee -a "$LOG"
        [ $? -ne 0 ] && { echo "$sddm_theme install has failed, please check the install.log"; }
      done
      echo "[Theme]\nCurrent=catppuccin" | sudo tee -a "$sddm_conf_dir/10-theme.conf" 2>&1 | tee -a "$LOG"
      break
      ;;
    t | T)
      echo "Installing Tokyo SDDM Theme"
      for sddm_theme in sddm-theme-tokyo-night; do
        install_package "$sddm_theme" 2>&1 | tee -a "$LOG"
        [ $? -ne 0 ] && { echo "$sddm_theme install has failed, please check the install.log"; }
      done
      echo "[Theme]\nCurrent=tokyo-night-sddm" | sudo tee -a "$sddm_conf_dir/10-theme.conf" 2>&1 | tee -a "$LOG"
      break
      ;;
    *)
      echo "Invalid choice. Please enter 'c' or 't'"
      continue
      ;;
    esac
  done
fi

clear
